import asyncio
from random import randint

from prometheus_client import REGISTRY, Counter, Histogram

from prometheus_client_utils.collectors import AsyncioCollector
from prometheus_client_utils.push_clients import PushGatewayClient

m_count = Counter('iters', 'count')
m_histogram = Histogram('iters_time', 'histogram')

semaphore = asyncio.Semaphore(50)


@m_histogram.time()
async def inner():
    async with semaphore:
        m_count.inc()
        await asyncio.sleep(randint(0, 10))


async def main():
    loop = asyncio.get_running_loop()

    REGISTRY.register(AsyncioCollector(loop))

    push_client = PushGatewayClient('localhost', 'test')
    push_client.schedule_push(5, loop)

    for i in range(10000):
        await asyncio.sleep(i / 100)
        loop.create_task(inner())


if __name__ == '__main__':
    asyncio.run(main())
