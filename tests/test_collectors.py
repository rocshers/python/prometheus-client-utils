import asyncio

import pytest
from prometheus_client.metrics_core import Metric

from prometheus_client_utils.collectors.asyncio_loop import AsyncioCollector
from prometheus_client_utils.collectors.push_clients import PushClientsCollector


class TestAsyncioCollector(object):
    @pytest.fixture()
    def collector(self):
        loop = asyncio.new_event_loop()
        return AsyncioCollector(loop)

    def test_init(self, collector: AsyncioCollector) -> None:
        assert isinstance(collector, AsyncioCollector)

    def test_collect(self, collector: AsyncioCollector) -> None:
        metrics = collector.collect()

        assert metrics

        for metric in metrics:
            assert isinstance(metric, Metric)


class TestPushClientsCollector(object):
    @pytest.fixture()
    def collector(self):
        return PushClientsCollector()

    def test_init(self, collector: PushClientsCollector) -> None:
        assert isinstance(collector, PushClientsCollector)

    def test_collect(self, collector: PushClientsCollector) -> None:
        metrics = collector.collect()

        assert metrics

        for metric in metrics:
            assert isinstance(metric, Metric)
